const Music = {
    audioElem: null,
    playOnStart: true,
    pollingFrequency: 2500,

    songList: [],
    activePlaylist: [],

    UserRequests: {
        PAUSE: "pause",
        PLAY: "play",
        NEXT: "next",
        PREV: "prev",
        CHANGE: "change",
        SEEK: "seek"
    },

    helpers: {
        changeAudioSource: function (filename) {
            try {
                Music.audioElem.src = `data/music/${ filename }`;
            } catch (e) {
                console.log('Cannot change source');
            }
        },

        getPlayingSongFilename: function () {
            return Music.audioElem.src.substr(Music.audioElem.src.lastIndexOf('/') + 1);
        }
    }
};

Music.init = function () {
    /* Get list of songs, and then good to go with everything else. */
    BL.httpGET('api/music/GetSongList.php', null, (xhr) => {
        try {
            this.songList = JSON.parse(xhr.responseText);
            this.setActivePlaylist(this.songList);

            this.audioElem = new Audio();
            this.addEventListeners();
            this.loadFromNowPlaying(this.playOnStart);
            setInterval(this.pollForUserRequest.bind(this), this.pollingFrequency);
        } catch (e) {
            console.error(e);
            console.log('Could not JSON parse GetSongList.php');
        }
    });
};

Music.addEventListeners = function () {
    this.audioElem.addEventListener('timeupdate', this.updateNowPlaying.bind(this));
    this.audioElem.addEventListener('durationchange', this.durationChangedHandler.bind(this));
    this.audioElem.addEventListener('ended', this.songEndedHandler.bind(this));

    /* If there was an error with the Audio object, it's likely that the source
    file was not found, so if that's the case, try to resolve by simply loading
    the first track in the playlist */
    this.audioElem.addEventListener('error', () => {
        console.log('Error');
        this.helpers.changeAudioSource(this.activePlaylist[0]);
    });
};

/* Makes a request to now-playing.json and applies the state to the audioElem */
Music.loadFromNowPlaying = function (playImmediately) {
    BL.httpGET('data/live/music/now-playing.json', null, (xhr) => {
        try {
            const nowPlayingState = JSON.parse(xhr.responseText);

            this.helpers.changeAudioSource(nowPlayingState.file);
            this.audioElem.currentTime = nowPlayingState.currentTime;

            playImmediately ? this.audioElem.play() : this.audioElem.pause();
        } catch (e) {
            console.error(e);
            console.log('Could not JSON parse now-playing.json');
        }
    });
};

/* Sends the current state of the player to the server and also updates the MiniMusicPlayer UI */
Music.updateNowPlaying = function (e) {
    const state = {
        file: this.audioElem.src.substr(this.audioElem.src.lastIndexOf('/') + 1),
        isPaused: this.audioElem.paused,
        currentTime: this.audioElem.currentTime,
        duration: this.audioElem.duration,
        playlist: this.activePlaylist
    };

    MiniMusicPlayer.update(state);
    BL.httpPOST('api/music/UpdateNowPlaying.php', state);
};

Music.pollForUserRequest = function (e) {
    const xhr = new XMLHttpRequest();

    xhr.onreadystatechange = () => {
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
            try {
                this.handleUserRequest(JSON.parse(xhr.responseText));
            } catch (e) {
                console.error(e);
                console.log('Could not JSON parse GetUserRequest.php');
            }
        }
    };

    xhr.open('get', 'api/music/GetUserRequest.php', true);
    xhr.send();
};

Music.setActivePlaylist = function (list) {
    this.activePlaylist = [];

    for (filename of list) {
        this.activePlaylist.push(filename);
    }

    if (this.audioElem) {
        this.updateNowPlaying();
    }
};


Music.handleUserRequest = function (userRequest) {
    switch (userRequest.type) {
        case this.UserRequests.CHANGE:

            if (this.audioElem.src != userRequest.changeTo) {
                this.helpers.changeAudioSource(userRequest.changeTo);
                this.audioElem.play();
            }

            break;
        case this.UserRequests.PAUSE:

            this.audioElem.pause();

            break;
        case this.UserRequests.PLAY:

            this.audioElem.play();

            break;
        case this.UserRequests.NEXT:

            this.stepTrack(true);

            break;
        case this.UserRequests.PREV:

            this.stepTrack(false);

            break;
        case this.UserRequests.SEEK:

            this.seek(userRequest.seekTo);

            break;
        default:
            console.log('Cannot handle UserRequest: ' + userRequest.type);
    }
};

Music.stepTrack = function (next) {
    const modifier = next ? 1 : -1;
    const playingSongFilename = this.helpers.getPlayingSongFilename();
    const indexOfPlayingSongInPlaylist = this.activePlaylist.indexOf(playingSongFilename);

    let indexOfSongToPlayInPlaylist = 0;

    if (indexOfPlayingSongInPlaylist != -1 && (this.activePlaylist.length - 1) !== indexOfPlayingSongInPlaylist) {
        indexOfSongToPlayInPlaylist = indexOfPlayingSongInPlaylist + modifier;
    }

    this.helpers.changeAudioSource(this.activePlaylist[indexOfSongToPlayInPlaylist]);
    this.audioElem.play();
};

Music.seek = function (seekTo) {
    this.audioElem.currentTime = seekTo;
};

Music.durationChangedHandler = function (e) {
    console.log('Duration changed');
};

Music.songEndedHandler = function (e) {
    this.stepTrack(true);
};
