const MiniMusicPlayer = {
    e: {
        playPauseBtn: document.getElementById('mini-music-player-btn-play-toggle'),
        nextBtn: document.getElementById('mini-music-player-btn-next'),
        songName: document.getElementById('mini-music-player-song-name'),
        timeProgress: document.getElementById('mini-music-player-duration-progress'),
        timeEnd: document.getElementById('mini-music-player-duration-end'),
        progressBar: document.getElementById('mini-music-player-duration-bar-fill')
    },

    helpers: {
        setProgressBar: function (percentage) {
            MiniMusicPlayer.e.progressBar.style.width = `${ percentage }%`;
        },

        getTimeDisplay: function (seconds) {
            return moment.duration(seconds, 'seconds').format('m:ss', { trim: false });
        }
    }
};

MiniMusicPlayer.init = function () {
    this.addEventListeners();
};

MiniMusicPlayer.addEventListeners = function () {
    const HOLD_THRESHOLD = 5;
    const SEEK_INTENSITY = 0.2;
    const SEEK_FREQUENCY = 100;

    let holdIntervalId = 0;
    let iterationsHeld = 0;
    let preventClick = false;

    function downHandler () {
        holdIntervalId = setInterval(function () {
            iterationsHeld++;

            if (iterationsHeld > HOLD_THRESHOLD) {
                preventClick = true;
                let diff = iterationsHeld - HOLD_THRESHOLD;
                Music.seek(Music.audioElem.currentTime + diff * SEEK_INTENSITY);
            }
        }, SEEK_FREQUENCY);
    }

    function upHandler () {
        clearInterval(holdIntervalId);
        iterationsHeld = 0;
        setTimeout(() => { preventClick = false }, 100);
    }

    this.e.nextBtn.addEventListener('touchstart', downHandler);
    this.e.nextBtn.addEventListener('mousedown', downHandler);
    this.e.nextBtn.addEventListener('touchend', upHandler);
    this.e.nextBtn.addEventListener('mouseup', upHandler);

    this.e.nextBtn.addEventListener('click', () => {
        if (!preventClick) {
            Music.handleUserRequest({ type: 'next' });
        }
    });

    this.e.playPauseBtn.addEventListener('click', () => {
        if (Music.audioElem.paused) {
            Music.handleUserRequest({ type: 'play' });
        } else {
            Music.handleUserRequest({ type: 'pause' });
        }
    });
};

MiniMusicPlayer.update = function ({ file, currentTime, duration, isPaused }) {
    this.e.songName.textContent = file;
    this.e.timeProgress.textContent = this.helpers.getTimeDisplay(currentTime);
    this.e.timeEnd.textContent = this.helpers.getTimeDisplay(duration);
    this.helpers.setProgressBar(Math.round(currentTime / duration * 100));

    this.e.playPauseBtn.querySelector('i').className = isPaused ? 'fa fa-play' : 'fa fa-pause';
};