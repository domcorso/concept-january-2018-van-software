const Data = {
    dataDir: 'data',
    appList: null
};

Data.init = function () {
    this.getAppList();
};

Data.getAppList = function () {
    const dataPath = `${ this.dataDir }/app_list.json`;

    const xhr = new XMLHttpRequest();

    xhr.onload = () => {
        try {
            this.appList = JSON.parse(xhr.responseText);
        }
        catch (e) {
            this.appList = [];
            console.error(
                `Could not parse JSON: ${ dataPath }`
            );
            console.error(e);
        }
    };

    xhr.open('get', dataPath, false);
    xhr.send();
};
