const Application = {};

/* Starts the clock in the top right corner of app */
Application.startClock = function () {
    const timeDisp = document.getElementById('time');
    const dateDisp = document.getElementById('date');

    function updateClock () {
        const datetime = moment();

        timeDisp.textContent = datetime.format('h:mm A');
        dateDisp.textContent = datetime.format('ddd Do MMMM YYYY');
    }

    updateClock();
    setInterval(updateClock, 1000);
};

/* Add miscellaneous listeners across the board */
Application.addListeners = function () {
    const appDrawerBtn = document.getElementById('app-drawer-btn');
    appDrawerBtn.addEventListener('click', (e) => {
        AppLoader.switchApp('app-drawer');
    });
};

Application.init = function () {
    this.startClock();
    this.addListeners();

    /* Initialize other modules */
    Data.init();
    AppLoader.init();
    MiniMusicPlayer.init();
    Music.init();
}

Application.init();
