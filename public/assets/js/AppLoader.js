const AppLoader = {
    defaultApp: 'app-drawer',

    errorLoadingAppHtml:
    `
        <div style="display: flex; align-items: center; justify-content: center; height: 100%">
            <img src="assets/images/covers/no-app-found.png" style="width: 80vw;" alt="No App Found">
        </div>
    `,

    helpers: {
        getAppNameFromUrl: function () {
            return window.location.hash.substr(1);
        }
    },

    e: {
        appCont: document.getElementById('app')
    }
};

AppLoader.init = function () {
    /* Add listener so we know if the user has changed the '#' value in the URL
    so then another app can be loaded */
    window.addEventListener('hashchange', this.loadFromUrl.bind(this));

    /*
        --- App Auto-Loading ---

        Check for an app title in the URL (after # (hash) symbol), if there
        is one, then load it
    */
    this.loadFromUrl();
};

AppLoader.switchApp = function (appName) {
    window.location.hash = '#' + appName;
};

/* Loads an app from the value after '#' in URL bar. If none, load default */
AppLoader.loadFromUrl = function () {
    const toLoad = this.helpers.getAppNameFromUrl() || this.defaultApp;

    if (toLoad) {
        this.load(toLoad);
    }
};

/* Makes a HTTP request to find the application with the given title */
AppLoader.load = function (appName) {
    /* Breakdown target path */
    const pathPrefix = 'apps/';
    const fileExt = '.php';
    const appPath = pathPrefix + appName + fileExt;

    /* Make HTTP Request */
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            this.handleAppResponse(xhr, appName, xhr.status != 200);
        }
    }.bind(this);

    /* Send request */
    xhr.open("get", appPath, true);
    xhr.send();
}

/* Callback function for the HTTP request made in 'load' */
AppLoader.handleAppResponse = function (reqObj, appName, errorLoading) {
    const animationSeconds = 0.5;
    const htmlContents = errorLoading ? this.errorLoadingAppHtml : reqObj.responseText;

    /* Run the animation */
    this.e.appCont.classList.add(appName, 'switch-app-animation');

    /* Load the app content at halfway of animation (this includes
        clearing the old content */
    setTimeout(() => {
        BL.clearChildren(this.e.appCont);
        this.e.appCont.innerHTML = htmlContents;

        /* Execute Javascript code for the app */
        try {
            eval(this.e.appCont.querySelector('#app-script').innerHTML);
        }
        catch (e) {
            console.warn(`${ appName } does not have a script.`);
        }
    }, animationSeconds * 500);

    /* Remove the animation class so the animation is ready for
    the next app switch */
    setTimeout(() => {
        this.e.appCont.classList.remove('switch-app-animation');
    }, animationSeconds * 1000);
}
