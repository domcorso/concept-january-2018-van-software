<?php

/* Path relative to the /public directory */
$directory_path = 'assets/css/app-specific';
$directory_listing = array_diff(scandir($directory_path), array('.', '..'));
$minified_css_listing = array_filter($directory_listing, 'is_minified_css');

foreach ($minified_css_listing as $file_name) {
    echo gen_link_tag($file_name) . "\n";
}

function is_minified_css ($file_name) {
    return strpos($file_name, '.min.css') !== false;
}

function gen_link_tag ($file_name) {
    return "<link rel=\"stylesheet\" href=\"assets/css/app-specific/$file_name\">";
}

?>
