<div class="swiper-container">
    <div id="swiper-wrapper" class="swiper-wrapper"></div>
</div>

<script type="text/javascript" id="app-script">

    const AppDrawer = {
        helpers: {
            genSwiperSlide: function () {
                const swiperSlide = document.createElement('div');
                swiperSlide.classList.add('swiper-slide');

                return swiperSlide;
            },

            genAppLink: function (app) {
                const iconPath = 'assets/images/app-drawer/icons';

                /* Create required elements */
                const appLink = document.createElement('div');
                const appIcon = document.createElement('img');
                const appName = document.createElement('div');

                /* Add classes */
                appLink.classList.add('app-link');
                appIcon.classList.add('app-icon');
                appName.classList.add('app-name');

                /* Setup app-icon <img /> */
                appIcon.src = `${ iconPath }/${ app.appName }.png`;
                appIcon.alt = app.displayName;

                /* Add app name text */
                appName.textContent = app.displayName;

                /* Append children */
                appLink.appendChild(appIcon);
                appLink.appendChild(appName);

                /* Add click listener */
                appIcon.addEventListener('click', (e) => {
                    AppLoader.switchApp(app.appName);
                });

                return appLink;
            }
        },

        e: {
            swiperWrapper: document.getElementById('swiper-wrapper')
        },

        swiperOptions: {
            effect: 'coverflow',
            coverflowEffect: {
                slideShadows: false
            }
        }
    };

    AppDrawer.init = function () {
        /* Check if the data we need is here */
        if (!Data.appList) {
            console.error('Could not initialize App Drawer, no app list data');
        }

        this.loadAppLinks();

        new Swiper ('.swiper-container', this.swiperOptions);
    };

    AppDrawer.loadAppLinks = function () {
        const swiperSlides = [];

        /* Create a maximum of 8 app icons in a single swiper slide */
        let currentSwiperSlide = this.helpers.genSwiperSlide();
        for (let i = 0; i < Data.appList.length; i++) {
            const app = Data.appList[i];
            const appLink = this.helpers.genAppLink(app);

            currentSwiperSlide.appendChild(appLink);

            /* If the current swiper slide is full, add it to array of
            swiper slides and make a new swiper slide */
            if (currentSwiperSlide.childElementCount == 8) {
                swiperSlides.push(currentSwiperSlide);
                currentSwiperSlide = this.helpers.genSwiperSlide();
            }

            /* If all apps are done, add the current swiper slide to array */
            if (i == Data.appList.length - 1) {
                swiperSlides.push(currentSwiperSlide);
            }
        }

        for (let swiperSlide of swiperSlides) {
            this.e.swiperWrapper.appendChild(swiperSlide);
        }
    };

    AppDrawer.init();
</script>
