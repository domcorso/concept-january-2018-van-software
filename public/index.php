<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Van Software</title>
        <link rel="stylesheet" href="assets/vendor/swiper/swiper.min.css">
        <script src="assets/vendor/moment/moment.min.js" charset="utf-8"></script>
        <script src="assets/vendor/moment/moment-duration-format.js" charset="utf-8"></script>
        <script src="assets/vendor/swiper/swiper.min.js" charset="utf-8"></script>
        <link rel="stylesheet" href="assets/css/master.css">
        <link rel="stylesheet" href="assets/vendor/fontawesome/css/font-awesome.min.css">
        <?php require_once('include/load_app_specific_css.php'); ?>
    </head>
    <body>
        <div id="container">
            <div id="top">
                <div id="datetime-container">
                    <div id="time"></div>
                    <div id="date"></div>
                </div>
                <div id="other-container">
                    <div id="mini-music-player">
                        <div id="mini-music-player-playing-status-cont">
                            <div id="mini-music-player-song-name-cont">
                                <marquee id="mini-music-player-song-name" scrollamount="3"></marquee>
                            </div>
                            <div id="mini-music-player-duration-cont">
                                <div id="mini-music-player-duration-progress">0:00</div>
                                <div id="mini-music-player-duration-bar">
                                    <div id="mini-music-player-duration-bar-fill"></div>
                                </div>
                                <div id="mini-music-player-duration-end">0:00</div>
                            </div>
                        </div>
                        <button type="button" id="mini-music-player-btn-next" class="app-icon"><i class="fa fa-step-forward"></i></button>
                        <button type="button" id="mini-music-player-btn-play-toggle" class="app-icon"><i class="fa fa-play"></i></button>
                    </div>
                </div>
            </div>
            <div id="bottom">
                <div id="app">

                </div>
                <div id="driver-buttons-cont">
                    <div class="app-link">
                        <img id="app-drawer-btn" class="app-icon" src="assets/images/app-drawer/icons/app-drawer.png" alt="Map">
                        <div class="app-name"></div>
                    </div>
                </div>
            </div>
        </div>

        <script src="assets/js/lib/BL.js" charset="utf-8"></script>
        <script src="assets/js/Data.js" charset="utf-8"></script>
        <script src="assets/js/AppLoader.js" charset="utf-8"></script>
        <script src="assets/js/MiniMusicPlayer.js" charset="utf-8"></script>
        <script src="assets/js/Music.js" charset="utf-8"></script>
        <script src="assets/js/Application.js" charset="utf-8"></script>
    </body>
</html>
